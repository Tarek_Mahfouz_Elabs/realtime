<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WebRTC Video Chat</title>

    <link href="https://fonts.googleapis.com/css2?family=Oxygen&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    @stack('styles')
</head>
<body>
<div class="container-fluid">

    <div class="row mb-3">
        <nav class="navbar navbar-light bg-light justify-content-between col-md-12">
            <div class="navbar-brand">
                @if(\Illuminate\Support\Facades\Auth::check())
                    <a class="navbar-brand" href="{{ route('home') }}">Video Conference</a>
                    &nbsp;-&nbsp;
                    {{ \Illuminate\Support\Facades\Auth::user()->email }}
                @else
                    <a class="navbar-brand" href="{{ route('home') }}">Video Conference</a>
                @endif
            </div>

            <div class="col-md-2">
                @if(\Illuminate\Support\Facades\Auth::check())
                    <a href="{{ route('room.create') }}">Create Room</a>
                    &nbsp;|&nbsp;
                    <a href="{{ route('logout') }}">Logout</a>
                @endif
            </div>
        </nav>
    </div>

    <div class="row">
        <div class="col-md-12 mr-1 ml-1">
            @yield('content')
        </div>
    </div>
</div>


@stack('scripts')
</body>
</html>
