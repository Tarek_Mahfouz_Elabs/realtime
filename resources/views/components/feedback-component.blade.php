@if(count($errors))
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <h6>
                <li>{{ $error }} </li>
            </h6>
        @endforeach
    </div>
@endif

@if(session()->has('success'))
    <div class="alert alert-success">
        <h3>
            {{ session()->get('success') }}
        </h3>
    </div>
@endif
