@extends('layouts.master')

@push('styles')
    <style>
        * {
            font-family: 'Oxygen', sans-serif;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>Room ID</th>
                    <th>Room Code</th>
                    <th>Room Participants</th>
                    <th>Join Room</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rooms as $room)
                    <tr>
                        <td>{{ $room->id }}</td>
                        <td>{{ $room->code }}</td>
                        <td>{{ count($room->users) }}</td>
                        <td>
                            <a href="{{ route('chat.room', $room->code) }}">Go</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
@endpush
