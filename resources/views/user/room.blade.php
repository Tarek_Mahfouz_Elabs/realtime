@extends('layouts.master')

@push('styles')
    <style>
        .message-feeds {
            min-height: 300px;
            max-height: 400px;
            border: 1px solid #eee;
            border-radius: 7px;
            overflow-y: scroll;
        }
        li {
            list-style: none;
        }
        .participants {
            height: 400px;
            overflow-y: scroll;
        }
        .user-card {
            border: 1px solid #eee;
            border-radius: 5px;
            margin-top: 10px;
            margin-left: 20px;
        }
        .msg {
            border: 1px solid #eee;
            border-radius: 7px;
            padding: 10px;
            margin: 10px;
        }
        .msg-form-me {
            text-align: right;
        }
        .msg-to-me {
            text-align: left;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <x-feedback-component />
        <div class="col-md-9 float-left">
            <div class="row mt-3 mb-3">
                <div id="message-feeds" class="message-feeds col-md-12">

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea name="message" id="message" rows="2" class="form-control col-md-12"></textarea>
                        <button type="submit" onclick="send()" class="btn btn-primary mt-1">Send</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 float-right participants">
            @foreach($users as $user)

                <div class="alert alert-success user-card">
                    <li>
                        <label for="">
                            <input id="participant_user" onchange="getMessages(this)" name="chat_with" type="radio" value="{{ $user->id }}"/>
                        </label>
                        <p>{{ $user->name }}</p>
                        <small>{{ $user->email }}</small>
                    </li>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script>
        var to_user_id = NaN;
        var user_id = "{{ auth()->user()->id }}";

        var messagesUrl = "{{ route('messages.list') }}";

        var sendUrl = "{{ route('messages.send') }}";
        var room_code = "{{ request()->room_code }}";

        function send() {
            let message = $('#message').val();

            if(!message.length || !to_user_id) {
                alert('something went wrong!')
                return;
            }
            $.ajax({
                url: sendUrl,
                method: 'POST',
                headers: {
                    Authorization: "Bearer {{ auth()->user()->api_token }}"
                },
                data: {
                    to_user_id: to_user_id,
                    message: message,
                    room_code: room_code
                }
            }).then(
                (response) => {
                    $('#message').val('');
                    let msg_feed = `<div class="msg msg-form-me alert-success">${message}</div>`;
                    $('#message-feeds').append(msg_feed);

                    console.log('response', response);
                },
                (error) => {
                    console.error('error', error);
                }
            );
        }

        function getMessages(e) {
            to_user_id = e.value;

            $.ajax({
                url: `${messagesUrl}?user_id=${e.value}&room_code=${room_code}`,
                method: 'GET',
                headers: {
                    Authorization: "Bearer {{ auth()->user()->api_token }}"
                },
            }).then(
                (response) => {
                    response.data.forEach((msg) => {
                        let msg_feed = '';
                        if(msg.from_user_id == user_id) {
                            msg_feed = `
                                <div class="msg msg-form-me alert-success">${msg.body}</div>
                            `;
                        } else {
                            msg_feed = `
                                <div class="msg msg-to-me alert-warning">${msg.body}</div>
                            `;
                        }
                        $('#message-feeds').append(msg_feed);
                    });
                    console.log('response', response.data);
                },
                (error) => {
                    console.error('error', error);
                }
            );
        }
    </script>
@endpush
