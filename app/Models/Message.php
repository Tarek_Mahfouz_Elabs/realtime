<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at',];

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id')->withDefault();
    }
    public function toUser()
    {
        return $this->belongsTo(User::class, 'to_user_id')->withDefault();
    }
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id')->withDefault();
    }

}
