<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    private $model;
    private $user;
    public function __construct(Message $model)
    {
        $this->model = $model;
        $this->user = Auth::guard('api')->user();
    }

    public function send(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'to_user_id' => 'required|exists:users,id',
                'message' => 'required',
                'room_code' => 'required|exists:rooms,code'
            ]);
            if($validator->fails()) {
                return jsonResponse($validator->errors(), 400, false);
            }

            $room = Room::where('code', $request->room_code)->first();

            $this->model->create([
                'from_user_id' => $this->user->id,
                'to_user_id' => $request->to_user_id,
                'body' => $request->message,
                'room_id' => $room->id
            ]);

        } catch (\Exception $e) {
            return jsonResponse([$e->getMessage()], 500, false);
        }

        return jsonResponse();
    }

    public function messages(Request $request)
    {
        $user = $this->user;
        try {
            if(!$room = Room::where('code', $request->room_code)->first()) {
                return jsonResponse([], 404, false);
            }
            $messages = $this->model
                ->where(function($query) use($request,$user) {
                    $query->where('to_user_id', $request->user_id);
                    $query->where('from_user_id',$user->id);
                })
                ->orWhere(function($query) use($request,$user) {
                    $query->where('from_user_id', $request->user_id);
                    $query->where('to_user_id',$user->id);
                })
                ->where('room_id', $room->id)
                ->get();

        } catch (\Exception $e) {
            return jsonResponse([$e->getMessage()], 500, false);
        }

        return jsonResponse($messages);
    }
}
