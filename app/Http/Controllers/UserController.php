<?php

namespace App\Http\Controllers;

use App\Models\Room;
use App\Models\RoomUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function submitLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);
        if($user = Auth::attempt($request->only(['email', 'password']))) {
            return redirect()->route('home');
        }

        return redirect('login')->withErrors('error email or password');
    }

    public function submitRegister(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:3',
        ]);

        $user = $this->model->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'api_token' => bcrypt(time()."$request->email&$request->password")
        ]);

        if($user && Auth::attempt($request->only(['email', 'password']))) {
            return redirect()->route('home');
        }

        return redirect()->back()->withErrors('data error!');
    }


    public function home()
    {
        // $my_rooms = Auth::user()->rooms;
        $rooms = Room::all();

        return view('user.home')->with([
            'rooms' => $rooms
        ]);
    }

    public function room($room_code)
    {
        if(!$room = Room::where('code', $room_code)->first()) {
            return redirect()->back();
        }
        RoomUser::updateOrCreate([
            'room_id' => $room->id,
            'user_id' => Auth::user()->id
        ]);

        $users = $room->users->where('id','!=',Auth::user()->id);

        return view('user.room')->with([
            'users' => $users
        ]);
    }

    public function createRoom()
    {
        return view('user.create-room');
    }

    public function storeRoom(Request $request)
    {
        \DB::beginTransaction();
        try {
            $room_name = str_replace(' ', '-', $request->name);

            $room_code = $this->createRoomCode($room_name, Auth::user()->id);

            $data = [
                'name' => $request->name,
                'user_id' => Auth::user()->id,
                'code' => $room_code
            ];

            $room = Room::create($data);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
        return redirect()->route('chat.room', $room_code);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    private function createRoomCode($name, $userID, $count = 4)
    {
        return time()."-$name-$userID-".rand(pow(10, (int)$count - 1), pow(10, (int)$count) - 1);
    }
}
