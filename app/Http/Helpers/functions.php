<?php


function jsonResponse($data = [], $code = 200, $status = true) {
    return response()->json([
        'status' => $status,
        'data' => $data
    ], $code);
}
