<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'UserController@login']);
    Route::post('login', ['as' => 'submit.login', 'uses' => 'UserController@submitLogin']);

    Route::get('register', ['as' => 'register', 'uses' => 'UserController@register']);
    Route::post('register', ['as' => 'submit.register', 'uses' => 'UserController@submitRegister']);

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/', ['as' => 'home', 'uses' => 'UserController@home']);

        Route::get('room/{room_code}', ['as' => 'chat.room', 'uses' => 'UserController@room']);

        Route::get('create-room', ['as' => 'room.create', 'uses' => 'UserController@createRoom']);
        Route::post('create-room', ['as' => 'room.store', 'uses' => 'UserController@storeRoom']);

        Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
    });
});
