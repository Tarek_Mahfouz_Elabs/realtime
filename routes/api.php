<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['namespace' => 'API'], function () {

    Route::post('login', ['as' => 'api.login', 'uses' => 'AuthController@login']);

    Route::group(['middleware' => 'auth:api', 'as' => 'messages.'], function() {
        Route::get('messages', ['as' => 'list', 'uses' => 'MessageController@messages']);
        Route::post('messages', ['as' => 'send', 'uses' => 'MessageController@send']);
    });
});
